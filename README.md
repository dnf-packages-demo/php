# php

Scripting language for creating dynamic web sites. https://apps.fedoraproject.org/packages/php

* https://repology.org/project/php/versions
* [Next version](https://blog.remirepo.net/post/2019/05/22/PHP-7.4-as-Software-Collection)
  * [Using Software Collections](https://www.softwarecollections.org/en/docs/)